package main

import (
	"fmt"
	"os"
	"strconv"

	"log"

	"github.com/urfave/cli/v2"
	"gitlab.com/matsto/my_photo_uploader/commands"
	"gitlab.com/matsto/my_photo_uploader/handlers"
)

func main() {
	c, err := commands.NewCommands()
	defer c.DB.Close()
	if err != nil {
		log.Fatal(err)
	}
	url, err := c.GetBaseUrl()
	if err != nil {
		log.Fatal(err)
	}

	h := handlers.NewHandler(url)

	token, err := c.LoadToken()
	if err != nil {
		log.Println(err)
	} else {
		h.AddToken(token)
	}

	app := &cli.App{
		Version: "0.0.1",
		Commands: []*cli.Command{
			{
				Name: "login",
				Action: func(cCtx *cli.Context) error {
					var username, password string
					fmt.Print("Type an username: ")
					fmt.Scan(&username)
					fmt.Print("Type a password: ")
					fmt.Scan(&password)
					err := h.Login(username, password)
					if err != nil {
						return err
					}
					c.SaveToken(h.ApiToken)
					return nil
				},
			},
			{
				Name:  "delete",
				Usage: "delete existing assets",
				Subcommands: []*cli.Command{
					{
						Name:  "gallery",
						Usage: "delete gallery",
						Action: func(cCtx *cli.Context) error {
							id := cCtx.Args().First()
							if id == "" {
								fmt.Print("Type a gallery id: ")
								fmt.Scan(&id)
							}
							err := h.DeleteGallery(id)
							if err != nil {
								return err
							}
							fmt.Printf("Gallery id: %s has been deleted\n", id)
							return nil
						},
					},
				},
			},
			{
				Name:  "new",
				Usage: "create new assets",
				Subcommands: []*cli.Command{
					{
						Name: "gallery",
						Action: func(cCtx *cli.Context) error {
							name := cCtx.Args().First()
							if name == "" {
								fmt.Print("Type a gallery name: ")
								fmt.Scan(&name)
							}
							gal, err := h.AddGallery(name)
							if err != nil {
								return err
							}
							fmt.Println(*gal)
							return nil
						},
					},
					{
						Name: "user",
						Action: func(cCtx *cli.Context) error {
							name := cCtx.Args().First()
							if name == "" {
								fmt.Print("Type a user name: ")
								fmt.Scan(&name)
							}
							email := cCtx.Args().Get(1)
							if email == "" {
								fmt.Print("Type an email: ")
								fmt.Scan(&email)
							}
							password := cCtx.Args().Get(2)
							if password == "" {
								fmt.Print("Type a password: ")
								fmt.Scan(&password)
							}
							user, err := h.AddUser(name, email, password)
							if err != nil {
								return err
							}
							fmt.Println(*user)
							return nil
						},
					},
				},
			},
			{
				Name:  "add",
				Usage: "add photos to a gallery",
				Action: func(ctx *cli.Context) error {
					galId, err := strconv.Atoi(ctx.Args().First())
					if err != nil {
						fmt.Print("Type a gallery ID: ")
						fmt.Scan(&galId)
					}
					folderPath := ctx.Args().Get(1)
					if folderPath == "" {
						fmt.Print("Type a folder path: ")
						fmt.Scan(&folderPath)
					}
					err = h.AddImages(galId, folderPath)
					if err != nil {
						return err
					}
					return nil
				},
			},
			{
				Name:  "link",
				Usage: "link a user to a gallery",
				Action: func(ctx *cli.Context) error {
					galId, err := strconv.Atoi(ctx.Args().First())
					if err != nil {
						fmt.Print("Type a gallery ID: ")
						fmt.Scan(&galId)
					}
					userId, err := strconv.Atoi(ctx.Args().Get(1))
					if err != nil {
						fmt.Print("Type a user ID: ")
						fmt.Scan(&userId)
					}
					link, err := h.AddUserToGallery(galId, userId)
					if err != nil {
						return err
					}
					fmt.Println(*link)
					return nil
				},
			},
		},
	}
	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}

}
