module gitlab.com/matsto/my_photo_uploader

go 1.21.4

require (
	github.com/gabriel-vasile/mimetype v1.4.3
	github.com/kolesa-team/go-webp v1.0.4
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/schollz/progressbar/v3 v3.14.1
)

require (
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/stretchr/testify v1.8.4 // indirect
	github.com/urfave/cli/v2 v2.27.1 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/term v0.16.0 // indirect
)
