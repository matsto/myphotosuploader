package database

import (
	"fmt"

	"github.com/boltdb/bolt"
)

type DB struct {
	db *bolt.DB
}
func (d *DB) Open() error {
	db, err := bolt.Open("my.db", 0600, nil)
	if err != nil {
		return err
	}
	d.db = db
	return nil
}

func (d *DB) Close() error {
	return d.db.Close()
}

func (d *DB) CreateBucket(name string) (*bolt.Bucket, error) {
	var (
		b   *bolt.Bucket
		err error
	)
	d.db.Update(func(tx *bolt.Tx) error {
		b, err = tx.CreateBucketIfNotExists([]byte(name))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})
	return b, err
}

func (d *DB) DeleteBucket(name string) error {
	err := d.db.Update(func(tx *bolt.Tx) error {
		err := tx.DeleteBucket([]byte(name))
		if err != nil {
			return fmt.Errorf("create bucket: %s", err)
		}
		return nil
	})
	return err
}

func (d *DB) Add(bucket, key, value string) error {
	err := d.db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		err := b.Put([]byte(key), []byte(value))
		return err
	})
	return err
}

func (d *DB) Get(bucket, key string) (string, error) {
	var value string
	err := d.db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucket))
		v := b.Get([]byte(key))
		if v == nil {
			return fmt.Errorf("key %s not found", key)
		}
		value = string(v)
		return nil
	})
	return value, err
}
