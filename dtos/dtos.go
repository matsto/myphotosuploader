package dtos

import "io/fs"

type (
	TokenDto struct {
		Token string `json:"token"`
	}

	GalleryDto struct {
		ID        int    `json:"id"`
		Name      string `json:"name"`
		CreatedAt string `json:"created_at"`
		Active    bool   `json:"active"`
	}

	PhotoDto struct {
		ID        int    `json:"id"`
		Order     int    `json:"order"`
		Etag      string `json:"etag"`
		GalleryID int    `json:"gallery_id"`
	}

	PhotoFile struct {
		Order int
		File  fs.DirEntry
	}

	GalleryLinkage struct {
		GalleryID int `json:"gallery_id"`
		UserID    int `json:"user_id"`
		ID        int `json:"id"`
	}

	User struct {
		ID        int    `json:"id"`
		Name      string `json:"name"`
		Email     string `json:"email"`
		Password  string `json:"password"`
		CreatedAt string `json:"created_at"`
	}
)
