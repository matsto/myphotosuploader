package commands

import (
	"fmt"
	"log"

	"gitlab.com/matsto/my_photo_uploader/database"
)

type Commands struct {
	DB                *database.DB
	CredentialsBucket string
	URLBucket         string
}

func (c *Commands) InitBuckets() error {
	_, err := c.DB.CreateBucket(c.CredentialsBucket)
	if err != nil {
		return err
	}
	_, err = c.DB.CreateBucket(c.URLBucket)
	if err != nil {
		return err
	}
	return nil
}

func (c *Commands) LoadToken() (string, error) {
	log.Println("Loading Token...")
	token, err := c.DB.Get(c.CredentialsBucket, "token")
	if err != nil {
		return "", err
	}
	if token == "" {
		return "", fmt.Errorf("token is empty")
	}
	return token, nil
}

func (c *Commands) SaveToken(token string) error {
	log.Println("Saving Token...")
	err := c.DB.Add(c.CredentialsBucket, "token", token)

	if err != nil {
		return err
	}

	return nil
}

func (c *Commands) loadBaseUrl() (string, error) {
	url, err := c.DB.Get(c.URLBucket, "url")
	if err != nil {
		return "", err
	}

	if url == "" {
		return "", fmt.Errorf("base URL is empty")
	}
	return url, nil
}

func (c *Commands) saveBaseUrl() (string, error) {
	var url string
	fmt.Print("Type a base URL: ")
	fmt.Scan(&url)
	if url == "" {
		return "", fmt.Errorf("base URL is empty")
	}

	err := c.DB.Add(c.URLBucket, "url", url)
	if err != nil {
		return "", err
	}

	return url, nil
}

func (c *Commands) GetBaseUrl() (string, error) {
	url, err := c.loadBaseUrl()
	if err != nil {
		url, err = c.saveBaseUrl()
		if err != nil {
			return "", err
		}
	}
	return url, nil
}

func NewCommands() (*Commands, error) {
	db := &database.DB{}
	err := db.Open()
	if err != nil {
		log.Fatal(err)
	}

	c := Commands{
		DB:                db,
		CredentialsBucket: "credentials",
		URLBucket:         "base_url",
	}
	err = c.InitBuckets()
	if err != nil {
		return nil, err
	}
	return &c, nil
}
