package handlers

import (
	"fmt"

	"gitlab.com/matsto/my_photo_uploader/dtos"
)

func (h *Handler) AddGallery(name string) (*dtos.GalleryDto, error) {
	d := map[string]interface{}{
		"name": name,
	}
	var output dtos.GalleryDto
	err := h.post("/api/galleries/", d, nil, &output)
	if err != nil {
		return nil, err
	}
	return &output, nil
}

func (h *Handler) DeleteGallery(id string) error {
	return h.delete(fmt.Sprintf("/api/galleries/%s", id), map[string]string{})
}
