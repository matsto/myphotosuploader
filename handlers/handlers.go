package handlers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"time"
)

type Handler struct {
	apiBaseUrl string
	ApiToken   string
	client     *http.Client
}

func NewHandler(apiBaseUrl string) *Handler {
	return &Handler{
		apiBaseUrl: apiBaseUrl,
		client:     &http.Client{Timeout: time.Second * 120},
	}
}

func (h *Handler) AddToken(token string) {
	h.ApiToken = token
}

func (h *Handler) serializeToJSON(data map[string]interface{}) ([]byte, error) {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return []byte{}, err
	}
	return jsonData, nil
}

func (h *Handler) buildUrl(path string) string {
	return h.apiBaseUrl + path
}

func (h *Handler) addHeaders(req *http.Request, headers map[string]string) {
	for key, value := range headers {
		req.Header.Set(key, value)
	}
	if req.Header.Get("Content-Type") == "" {
		req.Header.Set("Content-Type", "application/json")
	}

	if h.ApiToken != "" {
		req.Header.Set("Authorization", "Bearer "+h.ApiToken)
	}
}

func (h *Handler) decodeBody(resp *http.Response, output interface{}) error {
	err := json.NewDecoder(resp.Body).Decode(output)
	if err != nil {
		return err
	}
	return nil
}

func (h *Handler) post(url string, body map[string]interface{}, headers map[string]string, output interface{}) error {
	var (
		data []byte
		err  error
	)
	if body != nil {
		data, err = h.serializeToJSON(body)
		if err != nil {
			return err
		}
	}
	req, err := http.NewRequest("POST", h.buildUrl(url), bytes.NewBuffer(data))
	if err != nil {
		return err
	}

	h.addHeaders(req, headers)

	resp, err := h.client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("HTTP error %d", resp.StatusCode)
	}
	defer resp.Body.Close()
	err = h.decodeBody(resp, output)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (h *Handler) postFile(url string, filename string, file *os.File, bodyData map[string]string, headers map[string]string, output interface{}) error {
	file.Seek(0, 0)

	// Create a new multipart writer
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)

	// Create a form file field
	fw, err := writer.CreateFormFile("photo", filename)
	if err != nil {
		return err
	}

	// Copy the file contents to the form file field
	_, err = io.Copy(fw, file)
	if err != nil {
		return err
	}

	// Add form fields if body exist
	for key, value := range bodyData {
		writer.WriteField(key, value)
		continue

	}

	// Close the multipart writer
	writer.Close()

	// Create the request with the multipart body
	req, err := http.NewRequest(http.MethodPost, h.buildUrl(url), bytes.NewReader(body.Bytes()))
	if err != nil {
		return err
	}

	headers["Content-Type"] = writer.FormDataContentType()
	// Set the content type header
	h.addHeaders(req, headers)

	// Send the request
	resp, err := h.client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("HTTP error %d", resp.StatusCode)
	}
	defer resp.Body.Close()

	err = h.decodeBody(resp, output)
	if err != nil {
		fmt.Println(err)
		return err
	}
	return nil
}

func (h *Handler) req(url string, headers map[string]string, method string) error {
	req, err := http.NewRequest(method, h.buildUrl(url), &bytes.Buffer{})
	if err != nil {
		return err
	}

	h.addHeaders(req, headers)

	resp, err := h.client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("HTTP error %d", resp.StatusCode)
	}
	return nil
}

func (h *Handler) delete(url string, headers map[string]string) error {
	return h.req(url, headers, http.MethodDelete)
}

func (h *Handler) get(url string, headers map[string]string) error {
	return h.req(url, headers, http.MethodGet)
}
