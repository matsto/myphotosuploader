package handlers

import (
	"fmt"

	"gitlab.com/matsto/my_photo_uploader/dtos"
)

func (h *Handler) AddUser(name string, email string, password string) (*dtos.User, error) {
	d := map[string]interface{}{
		"name":     name,
		"email":    email,
		"password": password,
	}
	var output dtos.User
	err := h.post("/api/users/", d, nil, &output)
	if err != nil {
		return nil, err
	}
	return &output, nil
}

func (h *Handler) AddUserToGallery(galleryId, userId int) (*dtos.GalleryLinkage, error) {
	d := map[string]interface{}{
		"user_id": userId,
	}
	var output dtos.GalleryLinkage
	err := h.post(fmt.Sprintf("/api/galleries/%d/users/", galleryId), d, nil, &output)
	if err != nil {
		return nil, err
	}
	return &output, nil
}
