package handlers

import (
	"fmt"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"

	"github.com/gabriel-vasile/mimetype"
	"github.com/schollz/progressbar/v3"
	"gitlab.com/matsto/my_photo_uploader/dtos"
)

func (h *Handler) AddImages(galleryId int, folderPath string) error {
	workerNo := runtime.NumCPU() * 2
	var (
		wg    sync.WaitGroup
		ch    = make(chan dtos.PhotoFile, workerNo*2)
		errCh = make(chan error)
	)
	url := fmt.Sprintf("/api/galleries/%d/photos/", galleryId)

	files, err := os.ReadDir(folderPath)
	if err != nil {
		return err
	}
	bar := progressbar.Default(int64(len(files)))

	go func() {
		for i, file := range files {
			ch <- dtos.PhotoFile{
				Order: i+1,
				File:  file,
			}
		}
		close(ch)
	}()

	for i := 0; i < workerNo; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for f := range ch {
				err := h.addImage(galleryId, f.Order, url, folderPath, f.File)
				if err != nil {
					errCh <- err
					return
				}
				bar.Add(1)
			}
		}()
	}

	go func() {
		wg.Wait()
		close(errCh)
	}()

	err = <-errCh
	if err != nil {
		close(ch)
		return err
	}

	return nil
}

func (h *Handler) addImage(galleryId, order int, url, folderPath string, file fs.DirEntry) error {
	if file.IsDir() {
		return nil
	}

	imagePath := filepath.Join(folderPath, file.Name())

	//detect mimetype
	mtype, err := mimetype.DetectFile(imagePath)
	if err != nil {
		log.Fatal(err)
	}

	mimeGrp := strings.Split(mtype.String(), "/")[0]
	if mimeGrp != "image" {
		log.Printf("Skipping file %s, mimetype %s", file.Name(), mtype.String())
		return nil
	}

	tempFile, err := os.CreateTemp("", "temp_*.webp")
	if err != nil {
		return err
	}
	defer tempFile.Close()

	defer os.Remove(tempFile.Name())

	// Convert image to webp
	err = convertToWebp(imagePath, tempFile)
	if err != nil {
		return err
	}
	var output dtos.PhotoDto

	// Upload webp image
	body := map[string]string{
		"order": fmt.Sprint(order),
	}
	filePathParts := strings.Split(file.Name(), ".")
	ext := filePathParts[len(filePathParts)-1]
	newName := strings.Replace(file.Name(), ext, "webp", 1)

	log.Printf("Uploading file: %s", newName)

	err = h.postFile(url, newName, tempFile, body, map[string]string{"Accept": "application/json"}, &output)
	if err != nil {
		return err
	}
	return nil
}
