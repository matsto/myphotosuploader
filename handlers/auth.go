package handlers

import "gitlab.com/matsto/my_photo_uploader/dtos"

func (h *Handler) Login(username string, password string) error {
	d := map[string]interface{}{
		"name":     username,
		"password": password,
	}
	var output dtos.TokenDto

	err := h.post("/api/auth/login/", d, nil, &output)
	if err != nil {
		return err
	}
	h.ApiToken = output.Token
	return nil
}
