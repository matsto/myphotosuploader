package handlers

import (
	"image"
	_ "image/jpeg"
	_ "image/png"

	"os"

	"github.com/kolesa-team/go-webp/encoder"
	"github.com/kolesa-team/go-webp/webp"
	"github.com/nfnt/resize"
)

func convertToWebp(filePath string, img *os.File) error {
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}

	i, _, err := image.Decode(file)
	if err != nil {
		return err
	}
	m := resize.Thumbnail(4000, 4000, i, resize.Lanczos3)

	options, err := encoder.NewLossyEncoderOptions(encoder.PresetDefault, 80)
	if err != nil {
		return err
	}

	if err := webp.Encode(img, m, options); err != nil {
		return err
	}
	return nil
}
